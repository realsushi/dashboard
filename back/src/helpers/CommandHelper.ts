import { ICommand, ICommandEvent } from "../interfaces/ICommand";


export function parseCommand(command: string): ICommandEvent | null {
    
    
    let params = command.split('"');
    let temp = params[0].split(' ');
    params[0] = temp[0];
    params.forEach((elem, index) => {
      if (elem == " ")
        params.splice(index, 1);
    })
  
    if (params[0][0] == '/') {
      let cmd = params[0].replace('/', '');
      params.shift();
  
      let parsedCommand: ICommandEvent = {
        command: cmd,
        args: params
      };
      return parsedCommand;
    } else {
      /** 
       *  error 
       */
      return null;
    }
  }
  
export function commandExist(commandList: ICommand[], command: ICommandEvent): ICommand | null {
    let cmd: ICommand = null;
    commandList.forEach(elem => {
      if (elem.command == command.command) {
        cmd = elem;
      }
    })
  
    return cmd;
  }

export function isFunction(functionToCheck) {
  return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}