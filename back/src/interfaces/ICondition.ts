export interface ICondition{
    key: string;
    condition?: string;
    value: any;
}