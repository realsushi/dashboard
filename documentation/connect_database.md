### Connection to the MariaDB/SQL database

1. The docker way (recommended)

```bash
docker exec -it database11 bash

root@a63df9545623:/'#'
mysql --user=root --password=root

Welcome to the MariaDB monitor.  Commands end with ; or \g.                                                                                              Your MariaDB connection id is 8                                                                                         Server version: 10.4.12-MariaDB-1:10.4.12+maria~bionic mariadb.org binary distribution                                                                                                                                                          Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.                                                                                                                                                                            Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.                                                                                                                                                                  MariaDB [(none)]>                                           
```

2. Good ol' way: use win10/Linux web stack - eg winginx/wamp/lamp
Start SQLD server daemon (requiring VC_Redist_C++ to start - see `choco install vcredist -all` on windows)  
Select `LOCALHOST` instead of new project on the winginx dashboard  
Goto `http://localhost:81`  