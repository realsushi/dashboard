### ContinuousIntegration project

1. Adding gitlab.ci on project
2. Set environment paths
3. Add Gitlab remote called `cicd`
4. `git push cicd branchfeature`

### Conclusion regarding CI in this small project:
1. Checks if each service can run `npm install`
2. Checks if each service can run `npm start`
3. Execute tests from every spec files on `npm test` for the Front service

---
### last interesting log for TESTING:

```bash
 Running with gitlab-runner 12.10.0-rc2 (6c8c540f)
   on docker-auto-scale ed2dce3a
Preparing the "docker+machine" executor
00:40
 Using Docker executor with image node:latest ...
 Pulling docker image node:latest ...
 Using docker image sha256:a5a6a9c328776cffabad04bf248e308c5efdd6d93b1e76166ac28a6695c71ccc for node:latest ...
Preparing environment
00:06
 Running on runner-ed2dce3a-project-18259761-concurrent-0 via runner-ed2dce3a-srm-1587641891-41910e27...
Getting source from Git repository
00:01
 $ eval "$CI_PRE_CLONE_SCRIPT"
 Fetching changes with git depth set to 50...
 Initialized empty Git repository in /builds/realsushi/dashboard/.git/
 Created fresh repository.
 From https://gitlab.com/realsushi/dashboard
  * [new ref]         refs/pipelines/139101401 -> refs/pipelines/139101401
  * [new branch]      ci                       -> origin/ci
 Checking out 7e9e53d6 as ci...
 Skipping Git submodules setup
Restoring cache
00:12
 Checking cache for default...
 Downloading cache.zip from https://storage.googleapis.com/gitlab-com-runners-cache/project/18259761/default 
 Successfully extracted cache
Downloading artifacts
00:14
 Downloading artifacts for install_dependencies (523697931)...
 Downloading artifacts from coordinator... ok        id=523697931 responseStatus=200 OK token=WkkBdt5L
Running before_script and script
 $ pwd
 /builds/realsushi/dashboard
 $ ls
 Databases
 README.md
 Scripts
 back
 containers-test
 docker-compose.yml
 documentation
 front
 launch-me.bat
 launch-me.sh
 $ cd front
 $ npm test
 > front@0.0.0 test /builds/realsushi/dashboard/front
 > ng test
 23 04 2020 11:40:22.076:WARN [karma]: No captured browser, open http://localhost:9876/
 23 04 2020 11:40:22.122:INFO [karma-server]: Karma v4.1.0 server started at http://0.0.0.0:9876/
 23 04 2020 11:40:22.123:INFO [launcher]: Launching browsers Chrome with concurrency unlimited
 23 04 2020 11:40:22.129:INFO [launcher]: Starting browser Chrome
 23 04 2020 11:40:22.130:ERROR [launcher]: No binary for Chrome browser on your platform.
   Please, set "CHROME_BIN" env variable.
 23 04 2020 11:40:31.131:WARN [karma]: No captured browser, open http://localhost:9876/
```

### Possible optimization for BUILD phase2:
It is copying 41601 by hand (node modules mostly)

```
Saving cache
00:20
 Creating cache default...
 front/node_modules/: found 41601 matching files    
 Uploading cache.zip to https://storage.googleapis.com/gitlab-com-runners-cache/project/18259761/default 
 Created cache
Uploading artifacts for successful job
00:21
 Uploading artifacts...
 front/node_modules/: found 41601 matching files    
 Uploading artifacts to coordinator... ok            id=523807299 responseStatus=201 Created token=rAHkSaWp
 Job succeeded
 ```

### Possible optimization for BUILD phase1:
It takes few minutes there
 ```bash
 Running with gitlab-runner 12.10.0-rc2 (6c8c540f)
   on docker-auto-scale 0277ea0f
Preparing the "docker+machine" executor
 Using Docker executor with image node:latest ...
 Pulling docker image node:latest ...
 ```