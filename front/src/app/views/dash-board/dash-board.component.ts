import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';
import { SettingsService } from 'src/app/services/settings.service';
@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.scss']
})
export class DashBoardComponent implements OnInit {

  constructor(
    private account: AccountService,
    public settings: SettingsService,
    ) { }

  ngOnInit() {

  }
}
