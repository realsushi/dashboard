const { Sequelize, Model, DataTypes } = require('sequelize');
const config = require('../../package.json');

const sequelize = new Sequelize(config.database.database, config.database.username, config.database.password, {
    host: config.database.host,
    dialect: 'mariadb'
})

export class User extends Model{}

User.init({
    id: {
      type: Sequelize.INTEGER, primaryKey: true
    },
    username: Sequelize.STRING
  }, { sequelize, modelName: 'users' })