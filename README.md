### Webapplication first demo

#### Start project
```s
docker-compose build
docker-compose up
```
### Project CI
Gitlab.CI YML file

### Test project
```shell
cd Front
npm test
```
browse [here](localhost:9876)

### try docker SAAS
browse [PWD](labs.play-with-docker.com)

##### Setup for Win10 dev env

#### Start the app:
`docker-compose up`
 
##### Remarks
* Compilation has been tested through scripts and containers and remotely thanks to Gitlab CI
* Linting has been used to find obvious bugs
* Manual testing capability - tests have been done with npm test locally
* E2E tests capability - not done
* We recommend using MS' VScode for Windows/Linux
* We've refactored code, please report any bug/bloatware as an issue.
* SQL is the equivalent of MariaDB
* Containers have been tested: frontend reach host, test for frontend yet, db is containerized
* Nodejs works flawlessly on Linux (Frontend & Backend)
* Application compiles on Linux too
* Tests have been passed on ARM64 architecture too
* Containers have been tested : Further testing on the backend and db is needed
* Docker compose has superseeded shell script
* dockerfile partially support database service yet.

##### SQL tips for error #1046
You need to tell MySQL which database to use:

`USE database_name;`

before you create a table.


#####  Developpment Setup for Win10

1. clone repo & install choco (see below)
2. `choco install winginx nodejs`
3. Open winginx toolbox and start a new project - random name or url in the input field
4. Clic on database tools and search for PHPMYADMIN
5. Browse to localhost:81 or project:81
6. Open PHPMYADMIN with username *[put usual linux master user in the field]* without filling the next field
7. Select **IMPORT** tab to import `database/poc.sql` and clic **GO**
8. If database is correctly imported, it should pop on the left panel under "poc", expand it to verify database import
9. Open Explorer.exe/Nautilus and browse to the cloned repository (usually $User/Documents/github/dashboard)
10. Execute build-me.bat to install and compile the application. (docker-compose build then up)
11. Browse to localhost:4200 to reach frontend

##### Project prerequisites
1. Press WIN+X to start a PowerShell with admin privileges
2. Type 
```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; `
  iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```
3. Type
```powershell
choco install winginx nodejs vcredist-all`
```
4. Close PowerShell admin once
5. Open a new PowerShell (Win+X / Win+R whatever...) without admin privileges

###### Further reading
[Choco install](https://jcutrer.com/windows/install-chocolatey-choco-windows10)
