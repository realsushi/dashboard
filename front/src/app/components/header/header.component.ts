import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SettingsService } from 'src/app/services/settings.service';
import { AccountService } from 'src/app/services/account.service';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  validatingForm: FormGroup;
  isConnected: Boolean = false;

  constructor(
    public account : AccountService,
    public settings: SettingsService,
    public http: HttpClient
    ) { }

  ngOnInit() {
    this.validatingForm = new FormGroup({
      signupFormModalName: new FormControl('', Validators.required),
      signupFormModalPassword: new FormControl(''),
    });
    if(this.account.user.username)
      this.isConnected = true;
    this.getWeather(43.6351,1.3970)
  }  

  connect() {
    console.log(this.signupFormModalName.value)
    this.account.user.username=this.signupFormModalName.value
    this.account.user.password=this.signupFormModalPassword.value
    this.account.login()
  }

  getWeather(longitude:number,latitude:number) {
    let url = `https://api.darksky.net/forecast/4424abe5b864a1b90ba6a4f921307f62/${longitude},${latitude}`
    const head = {
      headers: new HttpHeaders({ 
        'Access-Control-Allow-Origin':'*',
      })
    };
    this.http.get(url,head).subscribe(data => {
      console.log(data)
      return data
    })
  }

  get signupFormModalName() : any {

    return this.validatingForm.get('signupFormModalName');
    this.connect();
    this.account.user.username = this.signupFormModalName;

    console.log(this.account.user.username);
  }

  get signupFormModalPassword() : any {
    return this.validatingForm.get('signupFormModalPassword');
    this.account.user.password = this.signupFormModalPassword;
    console.log(this.account.user.password);
  }
}
