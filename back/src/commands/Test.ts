import { ICommand, ICommandSettings } from "../interfaces/ICommand";

export class Test implements ICommand{
    command = "test";

    description = "Test, there is no description.";

    onCommand(args: string[], settings: ICommandSettings) {

        settings.commandManager.execCommand('channel-create "aperotest5"', settings.socket, () => {
            settings.commandManager.execCommand("help", settings.socket);
        });
    }
}