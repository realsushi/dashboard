
import { UserManager } from './../managers/UserManager';
import { CommandManager } from "../managers/CommandManager";
import { SocketManager } from '../managers/SocketManager';

export interface ICommand{
    command: string;
    description: string;
    onCommand(args: string[], settings: ICommandSettings, callback?: any): IManagers | any;
}

export interface ICommandEvent{
    command: string;
    args: string[];
}

export interface IManagers{
    commandManager: CommandManager;
    userManager: UserManager;
    socketManager: SocketManager;
}

export interface ICommandSettings{
    commandManager: CommandManager;
    userManager: UserManager;
    socket: any;
    socketManager: SocketManager;
}