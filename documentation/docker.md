### Whole project and services
```shell
docker-compose up
```

### Frontedn Dockerfile
```shell
cd Front
docker build -t 'frontend1' .
docker run -p 4200:4200 -d frontend1
```

### Backend Dockerfile
```shell
cd Back
docker build -t 'backend1' .
docker run -p 3306:3000 -d backend1
```