import { IManagers } from './../interfaces/ICommand';
import { ICommand, ICommandSettings } from "../interfaces/ICommand";
import { IChannel } from "../interfaces/IChannel";
import { isFunction } from "util";
import { IMessage } from "../interfaces/IEvent";
import { IMessageCommand } from "../interfaces/IEvent";

export class ChannelUsers implements ICommand {
    command = "channel-users";

    description = "Displays the current users in the given channel | Usage: /" + this.command + " [channel]";

    onCommand(args: string[], settings: ICommandSettings, callback?: () => {}) {
        let name = args[0];
        
        let socketData = settings.socketManager.FindSocketById(settings.socket.id);
        
        let managers: IManagers = {
            userManager: settings.userManager,
            socketManager: settings.socketManager,
            commandManager: settings.commandManager,
        };
        return managers;
    }
}