### Update project
#### Structure of the Node
* Frontend: Angular + testing + extra
* Backend: Express SocketIo

Running Cypress testing would require updating Node to 10.13 and Angular to 9
```
ng update @angular/core @angular/cli
ng update @angular/material
npm install npm@latest
```
---
### don't forget to clear CACHE
---
[Further reading](https://update.angular.io/#8.0:9.0)