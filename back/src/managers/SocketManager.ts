import { ISocket } from './../interfaces/ISocket';
import { IEvent } from '../interfaces/IEvent';

export class SocketManager{

    socketList: ISocket[] = [];

    OnNewSocket(socket: ISocket){
        this.socketList.push(socket);
    }

    OnDisconnectSocket(socketId: string){
        this.socketList.forEach((socket, index) => {
            if (socket.socket.id == socketId){
                if (typeof socket.bindedData.user !== 'undefined'){
                    if (typeof socket.bindedData.user.username !== 'undefined'){
                        let username = socket.bindedData.user.username;
                        this.socketList.splice(index, 1);
                        
                        this.socketList.forEach((_socket) => {
                            let event: IEvent = {
                                name: "OnPlayerLeaveServer",
                                content: {
                                    username: username
                                }
                            }
                            
                            _socket.socket.emit('pingEvent', event);
                        });
                    }
                }
            }
        });
    }

    UpdateSocketData(socketId: string, keyValue: string, _object: any){
        this.socketList.forEach(_socket => {
            if (_socket.socket.id == socketId) {
                _socket.bindedData[keyValue] = _object;
            }
        });
    }

    FindSocket(keyObject: string, keyValue: string, value: string) : ISocket | null{
        let socket: ISocket = null;

        this.socketList.forEach(_socket => {
            if (typeof _socket.bindedData[keyObject][keyValue] !== undefined){
                if (_socket.bindedData[keyObject][keyValue] == value){
                    socket = _socket;
                }
            }
        })

        return socket;
    }

    FindSocketById(socketId: string) : ISocket | null{
        let socket: ISocket = null;

        this.socketList.forEach(_socket => {
            if (_socket.socket.id == socketId) {
                socket = _socket;
            }
        })

        return socket;
    }
}