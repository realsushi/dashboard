### Build fatal errors - how to fix
1. Define if it crashes using script
2. Test docker-compose
3. Test git push with Gitlab CI

* If it crashes with docker `docker-compose build --no-cache`

* npm cleaning
```s
cd front
npm cache clean
cd ..
```

* Read the error/warning and google-fu it.
