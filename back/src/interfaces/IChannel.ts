export interface IChannel{
    id?: number;
    uid?: string;
    description?: string;
    subtitle?: string;
    cover?: string;
}