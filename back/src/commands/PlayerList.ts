import { IEvent } from './../interfaces/IEvent';
import { ICommand, ICommandSettings } from "../interfaces/ICommand";
import { isFunction } from "util";

export class CreateChannel implements ICommand{
    command = "player-list";

    description = "List all players | Usage: /"+ this.command;

    onCommand(args: string[], settings: ICommandSettings, callback?: () => {}) {
        
        let users = settings.userManager.connectedUser;

        settings.socket.emit('message', users)
    }
}