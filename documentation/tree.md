* 84857fe (HEAD -> ci, cicd/ci) ADD - gitlabCI before_script npm install to fix Eacccess
* bfee36b BUG - npm eaccess issue
* b56c99f MOD - demo bertrand gitlab CI
* e9dae9d FIX - GitlabCI - changed docker image
* 59c36fb (origin/ci) FIX - temporary Gitlab-CI build
* df6ae54 BUG - GitlabCI missing browser - add manual chrome install
* e1c796d BUG - gitlabCI missing remote browser - removing ENV path with npm rebuild
* f7dd796 BUG - no remote browser - adding npm rebuild
* edcee36 BUG - brwoser not captured remotely - adding debug path && possible issue being chrome not in
stalled
* ab1b657 ADD - EXTRA ENV for gitlab_CI - stored local tests output in documentation
* 3384e02 (origin/tests, tests) MOD - typo in chromeheadless
* a53b514 BUG - browser not captured remotely - added custom launcher for chromeheadless
* b4d1790 BUG - browser not captured remotely - reverted to karma conf with extended parameters
* af02a1b BUG - browser not captured remotely > changed karma conf once again
* ca5a233 BUG - browser not captured remotely - karma with chromium
* 1a32e5c BUG - browser not catched on remote docker only - removed karma config
* e46f89b FIX - success : tests have failed > switching again to chromeHeadless
* 149f524 FIX - local test ok - removing chrome path
* 176c284 BUG - browser not captured : env path modified
* 7280056 BUG - browser not captured
* ade768d ADD - package pupeeter to improve CI execution
* 62fdc3c FIX - browser package missing > karma-runner dot gihub dot io
* a406787 FIX - ng test > npm test (rollback)
* 5d3c20c FIX - used export instead of env for var assign
* 59eaadd BUG - Chrome Env - setting ENV
* 180ea90 BUG - no ENV CHROME_BIN > changing docker test to ngcontainer
* 29b3511 MOD - karma conf modified for browser flag
* 4d25139 ADD - option for ng test headlessbrowser
| *   2a61bad (origin/develop, cicd/develop, develop) Merge branch 'ci' into develop
| |\
| |/
|/|
* | 50149d1 ADD - GitlabCI documentation
* | 97ef712 FIX - gitlab CI works for build
* | cc7832a BUG - error ng not found - changing path
* | 7e9e53d BUG - error ng not found  && passed line 96 to 100
* | bfcee07 BUG - folder has LoWeRcAsE name on GitLabCi
* | c17f821 BUG - pipeline failed line96
* | 6eb615b FIX - attempt number 561  to fix the issue
* | 0304666 FIX - targeting frontend poject folder for gitlab CI
* | e810cab FIX - CI testing root instead of Frontend
* |   057d471 (cicd/tests) merge develop
|\ \
* | | ed66988 MOD - updated README.md
* | | 030c940 ADD - testing gitlab-ci
| | * 1cf7784 (github/develop) FIX - rolled back to deprecated script and adding deprecated mention for
 compatibility
| |/
| | * e6d0507 (origin/docker, cicd/docker, docker) ADD - feature test db service from dockerhub
| |/
| *   fd8b46e (origin/master, github/master, cicd/master, master) Merge branch 'docker' into develop
| |\
| | * 69555ad ADD - dist to gitignore && updated documentation && updated script for project lift-off
ged
| | * 92e8904 Fix - docker-compose up - volumes are now declared
| | | * e8b4bdf (origin/front, front) Merge branch 'lucas-dev' into front
| | |/|
| |/| |
| | | * 9c3722b (origin/lucas-dev, github/lucas-dev, lucas-dev) add: working rest
| * | | 0c2f7c5 Merge branch 'docker' into develop
| |\| |
| | * | 1e6a392 ADD - docker backend - no error (1 fixed regarding port)
| | * | 4844745 MOD - compose skeleton
| * | |   422034b Merge branch 'tests' into develop
| |\ \ \
| |/ / /
|/| | |
* | | | ab54907 ADD - Summary of testing technologies
* | | | cd5b6fc ADD - basic automation testing script
| * | | cfdb3d5 Merge branch 'docker' into develop
| |\| |
| | * | ca0cda6 ADD - Nasty docker fix for frontend
| | * | d09935b ADD - fixed scope issue on containers by moving containers to folders - as services
| * | |   b061ea9 Merge branch 'back' into develop
| |\ \ \
 usability
| * | | |   defc332 Merge branch 'docker' into develop
| |\ \ \ \
| | | |/ /
| | |/| |
| | * | | 2c0e35d ADD - init branch docker with templates
| * | | |   213f299 Merge branch 'tests' into develop
| |\ \ \ \
| |/ / / /
|/| | | |
* | | | | 4e9ade9 ADD - init branch tests with doc file
| |/ / /
|/| | |
| * | |   15d89da Merge branch 'front' into develop
| |\ \ \
| | |/ /
| |/| |
| | * | a25a1a3 ADD - init branch front with mockup goal
| |/ /
|/| |
| * | 88f56e8 ADD - init branch back with docu roadmap
|/ /
* |   3da5dfb Merge branch 'Bertrand' of https://github.com/Alsushi/dashboard
|\ \
| * | 9d975c7 (origin/Bertrand, github/Bertrand, Bertrand) [TEST] First widget
| * |   80049cc Merge branch 'lucas-dev' of https://github.com/Alsushi/dashboard into Bertrand
| |\ \
| * | | c61eb45 FIRST COMMIT : Add Documentation
* | | |   bce72cb Merge branch 'lucas-dev' of https://github.com/Alsushi/dashboard
|\ \ \ \
| | |_|/
| |/| |
| * | | 88d4db9 [ADD] weather iframe
| | |/
| |/|
| * | 2204a95 [ADD] componant and bootstrap
| |/
| | * 6caaf88 (github/etienne) MOD - Git doc for branch recovery + DB delete empty stuff
| |/
|/|
* | 152efaf MOD - cleanup build scripts comments
|/
* 14b6894 ADD - Proof of concept for web application + Edit Readme.md + Autoload db
* 1bd30af MOD - Angular frontend cleanup for proof of concept app
* b471d35 add - project init with Angular + NodeJs Express