import { OnPlayerJoinServer, IEvent, OnServerAcceptConnection } from '../interfaces/IServer';
import { Injectable } from '@angular/core';
import { IUser } from '../interfaces/IUser';
import { Socket } from 'ngx-socket-io';
import { IEventable } from '../interfaces/IEventable';

@Injectable({
  providedIn: 'root'
})
export class AccountService implements IEventable{

    user: IUser = {
        id: 1,
        username: ""
    };

    constructor(public socket: Socket){
        this.user.username = prompt('Username please');

        this.registerEvent();

        console.log(this.user.username);

        // let users: IUser = {
        //     users: "/accounts-list",
        //     username: this.account.user.username
        // };
        // this.socket.emit('message', msg);

        this.login();
    }

    registerEvent(){
        this.socket.fromEvent(OnServerAcceptConnection).subscribe((event: IEvent) => {
            console.log("ldr")
            this.OnServerAcceptConnection(event);
        });
    }

    OnServerAcceptConnection(event: IEvent){
        console.log("You are connected !");
        console.log(event)
    }

    login(){
        this.registerEvent();
        let event: IEvent = {
            name: OnPlayerJoinServer,
            content: this.user
        };

        this.socket.emit('event', event);
    }

}
