import { ICondition } from "../interfaces/ICondition";

const config = require('../../package.json');
const NoSQL = require('nosql');
const db = NoSQL.load(config.databaseFile);


export class Database{
    findCondition(conditions: ICondition[]){
        return new Promise((resolve, reject) => {
            db.find().make((filter) => {
                conditions.forEach(condition => {
                    if (typeof condition.condition !== undefined) {
                        filter.where(condition.key, condition.condition, condition.value);
                    } else {
                        filter.where(condition.key, '=', condition.value);
                    }
                    filter.callback((err, response) => {
                        resolve({err, reject})
                    })
                })
            });
        })
    }
}