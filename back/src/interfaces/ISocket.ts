import { User } from "../models/User";

export interface ISocket{
    socket: any;
    bindedData: {
        user?: User;
    }
}