import { ICommand, ICommandSettings, IManagers } from './../interfaces/ICommand';
import { parseCommand, commandExist } from '../helpers/CommandHelper';
import { isFunction } from 'util';
import { IEvent } from '../interfaces/IEvent';
import { IServerState } from '../interfaces/IServerState';
import { UserManager } from './UserManager';
import { SocketManager } from './SocketManager';

export class CommandManager implements IServerState{

    commandList: ICommand[] = [];

    userManager: UserManager = null;
    socketManager: SocketManager = null;

    constructor(us: UserManager, som: SocketManager){
        this.userManager = us;
        this.socketManager = som;
    }

    setCommandList(commands: ICommand[]){
        this.commandList = commands;
    }

    execCommand(message: string, socket: any, callback?: any){
        message = '/' + message;
        console.log(message);
        let parsedCommand = parseCommand(message);

        if (parsedCommand != null) {
            let command = commandExist(this.commandList, parsedCommand);

            if (command != null) {
                let settings: ICommandSettings = {
                    commandManager: this,
                    userManager: this.userManager,
                    socket: socket,
                    socketManager: this.socketManager
                };
                
                if (isFunction(callback))
                    command.onCommand(parsedCommand.args, settings, callback);
                else
                    command.onCommand(parsedCommand.args, settings);
            } else {
                console.log("unknown command sorry");
            }
        }
    }

    getHelpUsages() : string[] {
        let helps: string[] = [];

        this.commandList.forEach(command => {
            helps.push(command.description);
        })

        return helps;
    }

    OnServerInit(){

    }

    OnServerSocketInit(socket: any){

    }

    OnEvent(managers: IManagers, socket: any, event: IEvent){
        
    }

}