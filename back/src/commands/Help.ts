import { ICommand, ICommandSettings } from "../interfaces/ICommand";
import { isFunction } from "util";

export class Help implements ICommand{
    command = "help";

    description = "Show help | Usage: /"+ this.command;

    onCommand(args: string[], settings: ICommandSettings, callback?: any) {
        let helps = settings.commandManager.getHelpUsages();

        helps.forEach(help => {
            settings.socket.emit('message', help);
        })

        if (isFunction(callback))
            callback();
    }
}