import { IManagers } from './../interfaces/ICommand';
import { OnServerAcceptConnection, OnPlayerJoinServer, OnPlayerLeaveServer, OnServerTestConnectionOfPlayer, OnPlayerTestConnection, IMessage } from './../interfaces/IEvent';
import { interval, Observable } from 'rxjs';
import { map, delay } from 'rxjs/operators';
import { IServerState } from './../interfaces/IServerState';
import { User } from "../models/User";
import { IUser } from "../interfaces/IUser";
import { IEvent } from '../interfaces/IEvent';
import { SocketManager } from './SocketManager';
import { ISocket } from '../interfaces/ISocket';

const config = require('../../package.json');

export class UserManager implements IServerState{
    
    connectedUser: User[] = [];

    socket: any = null;

    OnServerInit(){
        
    }

    OnServerSocketInit(socket: any){
        this.socket = socket;
    }

    OnEvent(managers: IManagers, _socket: any, event: IEvent) : IManagers | any{
        
        let socket: ISocket = managers.socketManager.FindSocketById(_socket.id);

        if (event.name == OnPlayerJoinServer){
            return this.OnPlayerJoinServer(managers, _socket, event)
        } else if (event.name == OnPlayerLeaveServer){
            return this.OnPlayerLeaveServer(managers, _socket, event)
        } else {
            return managers;
        }
    }

    OnPlayerJoinServer(managers: IManagers, socket: any, event: IEvent): IManagers{
        let username = event.content.username;

        if (username != null){
            User.findAll({ where: {username: username} }).then((users) => {
                if (typeof users[0] !== undefined){
                    managers.socketManager.UpdateSocketData(socket.id, 'user', users[0]);

                    let date = new Date();
                    let msg : IMessage = {
                        id: -1,
                        username: "Serveur",
                        message: "L'utilisateur " + username + " vient de se connecter",
                        date: date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " à " + date.getHours() + ":" + date.getMinutes()
                    }

                    socket.broadcast.emit('message', msg);
                    socket.emit('message', msg);

                    return managers;
                    // socket.emit('message', "L'utilisateur " + username + " viens de se connecter");
                }
            }).catch(err => {
                console.log(err);
                return managers;
            })
        } else {
            return managers;
        }
        

        
    }

    OnPlayerLeaveServer(managers: IManagers, socket: ISocket, event: IEvent){
        let username = event.content.username;
        return managers;
    }
}