import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
//import { ChannelService } from 'src/app/services/channel.service';
import { NotifierService } from 'angular-notifier';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-create-channel-modal',
  templateUrl: './create-channel-modal.component.html',
  styleUrls: ['./create-channel-modal.component.scss']
})
export class CreateChannelModalComponent implements OnInit {

  @ViewChild('uid', { static: true }) uid: ElementRef;
  @ViewChild('description', { static: true }) description: ElementRef;
  @ViewChild('subtitle', { static: true }) subtitle: ElementRef;
  @ViewChild('cover', { static: true }) cover: ElementRef;

  private readonly notifier: NotifierService;

  constructor(
    //private channelService: ChannelService,
    private notifierService: NotifierService,
    public dialogRef: MatDialogRef<CreateChannelModalComponent>
    ) {
      this.notifier = notifierService;
     }

  ngOnInit() {
  }

  createChannel(){
    //this.channelService.createChannel(
    //  this.uid.nativeElement.value,
    //  this.description.nativeElement.value,
    //  this.subtitle.nativeElement.value,
    //  this.cover.nativeElement.value,
    //)

    this.notifier.notify("success", "Le channel " +  this.uid.nativeElement.value + " à été créé");
  
    this.dialogRef.close();
  }
}
