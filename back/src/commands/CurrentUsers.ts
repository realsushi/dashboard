import { IManagers } from './../interfaces/ICommand';
import { ICommand, ICommandSettings } from "../interfaces/ICommand";
import { isFunction } from "util";
import { IMessage, IMessageCommand } from "../interfaces/IEvent";

export class CurrentUsers implements ICommand {
    command = "current-users";

    description = "Gives the users list of the current channel | Usage: /" + this.command;

    onCommand(args: string[], settings: ICommandSettings, callback?: () => {}) {    
        let socketData = settings.socketManager.FindSocketById(settings.socket.id);

        if (socketData != null) {
            let username = socketData.bindedData.user.username;
            let lobby = null;
            
            let msg : IMessageCommand = {command: this.command, content: lobby.users };
            settings.socket.emit('messageCommand', msg)
        }

        let managers: IManagers = {
            userManager: settings.userManager,
            socketManager: settings.socketManager,
            commandManager: settings.commandManager,
        };
        return managers;
    }
}