import { IEvent } from './IEvent';
import { SocketManager } from '../managers/SocketManager';
import { IManagers } from './ICommand';

export interface IServerState{
    OnServerInit(): any;
    OnServerSocketInit(socket): any;
    OnEvent(managers: IManagers, socket: any, event: IEvent): IManagers | any;
}
