import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

const config: SocketIoConfig = { url: 'http://localhost:3000', options: {} };

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashBoardComponent } from './views/dash-board/dash-board.component';
import { HeaderComponent } from './components/header/header.component';
import { SettingsService } from './services/settings.service';
import { AccountService } from './services/account.service';
import { CreateChannelModalComponent } from './modals/create-channel-modal/create-channel-modal.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatInputModule} from '@angular/material/input';
import {FormsModule, FormControl, FormGroup, Validators, ReactiveFormsModule} from "@angular/forms";
import { MatLabel } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http'; 
import { NotifierModule } from "angular-notifier";


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashBoardComponent,
    CreateChannelModalComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    SocketIoModule.forRoot(config),
    MDBBootstrapModule.forRoot(),
    AppRoutingModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    NotifierModule
  ],
  providers: [SettingsService, AccountService],
  bootstrap: [AppComponent],
  entryComponents: [
    CreateChannelModalComponent
  ]
})
export class AppModule { }
