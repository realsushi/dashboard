import { SocketManager } from './managers/SocketManager';
/** IMPORT CONFIGURATION */
const config = require('../package.json');

/** IMPORT TOOLS */
const { Sequelize, Model, DataTypes } = require('sequelize');
import { parseCommand, commandExist } from "./helpers/CommandHelper";

/** IMPORT INTERFACES */
import { ICommand, ICommandEvent, ICommandSettings, IManagers } from "./interfaces/ICommand";

/** IMPORT MODELS */
import { User } from "./models/User";

/** IMPORT MANAGERS */
import { CommandManager } from "./managers/CommandManager";

/** IMPORT COMMANDS */
import { Help } from "./commands/Help";
import { Test } from "./commands/Test";
import { UserManager } from "./managers/UserManager";
import { IEvent, IMessage } from "./interfaces/IEvent";
import { ISocket } from './interfaces/ISocket';
import { CurrentUsers } from './commands/CurrentUsers';
import { ChannelUsers } from './commands/ChannelUsers'


/** DECLARE SERVICES */
let app = require('express')();
let http = require('http').createServer(app);
let io = require('socket.io')(http);

let avaiblesCommands: ICommand[] = [];

/** DECLARE MANAGERS */
let socketManager: SocketManager = null;
let commandManager: CommandManager = null;
let userManager: UserManager = null;

/** SERVER INITITALISATION */
initManagers();
registerCommand();

function initManagers() {
  socketManager = new SocketManager();
  userManager = new UserManager();
  commandManager = new CommandManager(userManager, socketManager);
  
  commandManager.OnServerInit();
  userManager.OnServerInit();
}

function registerCommand() {
  avaiblesCommands.push(new CurrentUsers());
  avaiblesCommands.push(new ChannelUsers());
  avaiblesCommands.push(new Help());
  avaiblesCommands.push(new Test());

  commandManager.setCommandList(avaiblesCommands);
}

setInterval(() => {
}, 5000);

io.on('connection', function (socket) {

  let newSocket: ISocket = {
    socket: socket,
    bindedData: {}
  };
  socketManager.OnNewSocket(newSocket);

  socket.on('disconnect', () => {
    socketManager.OnDisconnectSocket(socket.id);
  });

  commandManager.OnServerSocketInit(socket);
  userManager.OnServerSocketInit(socket);

  socket.on('event', (event: IEvent) => {
    let managers: IManagers = {
      commandManager: commandManager,
      userManager: userManager,
      socketManager: socketManager,
    }
    let newManagers = userManager.OnEvent(managers, socket, event);
    if (typeof newManagers !== 'undefined'){
      assignManagers(newManagers);
    }
  });

  socket.on('pongEvent', (event: IEvent) => {
    let managers: IManagers = {
      commandManager: commandManager,
      userManager: userManager,
      socketManager: socketManager,
    }
    let newManagers = userManager.OnEvent(managers, socket, event);
    if (typeof newManagers !== 'undefined'){
      assignManagers(newManagers);
    }
  });

  socket.on('message', (message: IMessage) => {
    let parsedCommand = parseCommand(message.message);

    if (parsedCommand != null) {
      let command = commandExist(avaiblesCommands, parsedCommand);

      if (command != null) {
        let settings: ICommandSettings = {
          commandManager: commandManager,
          userManager: userManager,
          socket: socket,
          socketManager: socketManager
        };

        let managers = command.onCommand(parsedCommand.args, settings, () => {});
        if (typeof managers !== 'undefined'){
          assignManagers(managers);
        }
      } else {
        console.log("La commande n'existe pas");
      }
    } else {
      sendMessage(socket, message);
    }
  });
});

http.listen(3000, function () {
  console.log('listening on *:3000');
});

function sendMessage(socket: any, message: IMessage){
  /**
   *
   *
   */

  if (message.date == null){
     let date = new Date();
     message.date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " à " + date.getHours() + ":" + date.getMinutes();
  }

  socket.broadcast.emit('message', message);
  socket.emit('message', message);

}

function assignManagers(managers: IManagers){
  userManager = managers.userManager;
  commandManager = managers.commandManager;
  socketManager = managers.socketManager;
}
