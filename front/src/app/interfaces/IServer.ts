export interface IEvent{
    name: string;
    content?: any;
}

export interface IMessage{
    id?: number;
    message: string;
    date?: string;
    username?: string;
    channelUid?: string;
};

export interface IMessageCommand{
    command: string;
    content: any;
};

/* EVENT SERVER -> SENDED */
export const OnServerAcceptConnection = "OnServerAcceptConnection";
export const OnServerTestConnectionOfPlayer = "OnServerTestConnectionOfPlayer";
export const OnServerCreateChannel = "OnServerCreateChannel";

/* EVENT PLAYER -> RECEIVED */
export const OnPlayerJoinServer = "OnPlayerJoinServer";
export const OnPlayerLeaveServer = "OnPlayerLeaveServer";
export const OnPlayerTestConnection = "OnPlayerTestConnection";