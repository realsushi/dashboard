import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService{

    themeSelected = "light";

    constructor() {

    }

    changeMode(){
        if (this.themeSelected == "light"){
            this.themeSelected = "dark";
        } else {
            this.themeSelected = "light";
        }
    }
}
